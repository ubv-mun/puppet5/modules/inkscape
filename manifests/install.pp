# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include inkscape::install
class inkscape::install {
  package { $inkscape::package_name:
    ensure => $inkscape::package_ensure,
  }
}
